import React, { Component } from 'react';


class NameForm extends React.Component {
  // ip address of your localhost 
  //ip = '192.168.177.199';

  // URL of the api endpoint 
  //apiEndpoint = `http://${this.ip}:3000/tasks`;
  apiEndpoint = 'http://localhost:3000/tasks';

  state = { task: [], isLoading: true, error: null };
  
  async componentDidMount() {
    // next 2 lines for text box submission
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    try {
      
      const response = await fetch(this.apiEndpoint);
      console.log(typeof response);
      const data = await response.json();


      console.log(data);
      //this.setState({ task: data.results, isLoading: false });
      this.setState({ task: data, isLoading: false });


    } catch (error) {
      this.setState({ error: error.message, isLoading: false });
    }
  }

  // for textbox 
  handleChange(event) {    
    this.setState({value: event.target.value});
  }


  


  // for textbox submit
  handleSubmit(event) {
    
    window.location.reload()
    //alert('Task submitted!: ' + this.state.value);

    console.log(this.state.value);
    console.log(typeof this.state.value);

    //post the newly created task to api 
    fetch(this.apiEndpoint, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        task: this.state.value
      })
      
    })
    
    //window.location.reload();

    // next 4 lines: refresh and get the current list of tasks
/*     const response = fetch(this.apiEndpoint);
    console.log(typeof response);
    const data = response.json();
    this.setState({ task: data, isLoading: false }); */
    //window.location.reload()
    event.preventDefault();
    
    
  }


  renderTask = () => {
    const { task, isLoading, error } = this.state;
    //console.log("hello");
    if (error) {
      return <div>{error}</div>;
    }

    if (isLoading) {
      return <div>Loading...</div>;
    }

    
    // return our list of tasks
    return task.map(theTask => (

          <div>
          <h4 class="card-title"> &nbsp;&nbsp;&nbsp;&nbsp;--&nbsp; "{theTask}" </h4> 
        </div>
    ));
  };


  // manage text box render 
  textBox = () => {
    
    const { task, isLoading, error } = this.state;
    
    //console.log("hello");
    if (error) {
      return <div>{error}</div>;
    }

    if (isLoading) {
      return <div>Loading...</div>;
    }


    // Body 
    
      
      return <div>
      
        <div class="card">
          <h1>A Simple todo list!</h1>
          <form onSubmit={this.handleSubmit}>
            <label>
              Enter Task:
              <input type="text" value={this.state.value} onChange={this.handleChange} />
            </label>
            <input type="submit" value="Submit" />
          </form>
          <h2>Your list of tasks as of {new Date().toLocaleTimeString()}:</h2>
          <div class="card-body">
            
          </div>
          
          
      </div>
        
      </div>
      

  };

  render() {
    // what is sent to client
    return <div>
            {this.textBox()}
            <div>
              {this.renderTask()}
            </div>
          </div>;

  }
}
export default NameForm;

# Sample Microservices; A simple todo list
The purpose of this project is to provide an example of what a simple 3-tier microservice-based architecture might look like.

This project leverages docker runtime and utilizes docker-compose to deploy the application easily.  

![visual](./img/visual.png)

 

There are a total of 3 microservices, run as docker containers that build up the app:  

## Microservice 1 / Tier 1: Presentation 

Uses react.js to provides as the rendered html for the client (todo list).  This microservice relies on the ability to access the API to read and update tasks on the todo list. Exposes and uses port 80 on the host. 

## Microservice 2 / Tier 2: Application
The purpose of this microservice, written in express.js to expose an API in which microservice 1 (frontend) can query to get. Exposes and uses port 3000 on the host. This container connects to a docker network called “backend” so that it can communicate with database. 

## Microservice 3 / Tier 3: Database 
The purpose of this database, which uses mongodb, is to holds the tasks information. The container does not expose any ports on the host, and relies on the docker network “backend” so that API can perform create and read operations in the database. 

### PREREQS: 

-	Docker Installed and Running: https://docs.docker.com/get-docker/
-	Docker-compose: https://docs.docker.com/compose/install/

# Setup:
Create 2 docker networks: 
```terminal
docker network create frontend
docker network create backend
```
The most importatant network is the "backend," as this will be attached to the API and Database to create private interactions between them.  

Clone the repository and navigate into the repo: 
```terminal 
git clone https://gitlab.com/sample-3-tier-web-app/sample-microservices.git && cd sample-microservices
```

While in the root of the repo, issue the command below to build and deploy the “simple todo list”:
```terminal
docker-compose up
```

Once you see similar output below:
```
frontend_1  | ℹ ｢wds｣: Project is running at http://172.18.0.2/
frontend_1  | ℹ ｢wds｣: webpack output is served from 
frontend_1  | ℹ ｢wds｣: Content not from webpack is served from /public
frontend_1  | ℹ ｢wds｣: 404s will fallback to /
frontend_1  | Starting the development server...
frontend_1  | 
frontend_1  | Compiled with warnings.
frontend_1  | 
frontend_1  | /src/App.js
frontend_1  |   Line 1:17:    'Component' is defined but never used      no-unused-vars
frontend_1  |   Line 106:13:  'task' is assigned a value but never used  no-unused-vars
```
Open a browser and navigate to localhost:80, and you should be able to interact with the simple todo list, similar to below:



![visual](./img/visual2.png)


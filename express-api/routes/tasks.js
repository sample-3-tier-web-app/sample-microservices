var express = require('express');
var router = express.Router();

// connect to mongodb database 
let MongoClient = require('mongodb').MongoClient;
var url = "mongodb://mongo:27017/";


// POST  backend:3000/tasks
// Purpose:  get list of tasks from the database 
// Parameters:  N/A
// Status: INCOMPLETE

router.get('/', function(req, res, next) {
  //tasks = ['laundry', 'meal prep', 'shower', 'clean bathroom'];
  //tasks = ['laundry', 'meal prep', 'shower'];
  //tasks = [''];

  
  // query database for all groups and list them here: 
  MongoClient.connect(url, function(err, db) {
    console.log('here')
    if (err) throw err;
    var dbo = db.db("mydb");  // use database 'mydb'
    mongodb_data = dbo.collection("tasks");   // collection 'tasks'
    mongodb_data.distinct(
        "task",  // querying for group 
        (function(err, docs){
              if(err){
                  return console.log(err);
              }
              if(docs){  
                  console.log(docs);
                  res.json(docs);  // print via http
                  
              }

        })

    );

    });


// send back array of tasks
//res.json(tasks);

});





// POST  backend:3000/tasks
// Purpose:  add a new task to the list / database 
// Parameters:  takes in string, which can be defined as a "task"
// Status: INCOMPLETE
router.post('/', (req, res) => {

  // save array of objects (sailor info) into array called sailors
  tasks = req.body.task;

  res.json(tasks);
  console.log(tasks);

  // Connect to database 
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("mydb");

    var myobj = { task: tasks };
    dbo.collection("tasks").insertOne(myobj, function(err, res) {
      if (err) throw err;
      console.log("1 document inserted: " + tasks);
      db.close();
    });
  });
  

});

module.exports = router;

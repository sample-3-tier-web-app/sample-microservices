#!/bin/bash
# Run this script once you have mongodb running on your system 
# This script will also drop the database if it already exists
function testMongoScript {
    mongo <<EOF
    use mydb
    // drop if it exists
    db.dropDatabase()
    use mydb

    // create a collections called users and groups
    db.createCollection('tasks')

    // print empty tasks 
    db.tasks.find().pretty()
EOF
}

testMongoScript

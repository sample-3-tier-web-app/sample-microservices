yum -y install git tmux mlocate wget vim unzip

## install mongodb 
echo "[mongodborg-4.2]                                                                                                                                                                                           
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/\$releasever/mongodb-org/4.2/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-4.2.asc" > /etc/yum.repos.d/mongodb-enterprise-4.2.repo

# install via yum (what version is needed?)
yum install -y mongodb-org

# start mongodb and ensure its on durring boot
systemctl start mongod.service
systemctl enable mongod.service
systemctl status mongod.service

###==== installing nodejs and getting dependencies

# install node (is there a way to do this via rhel?)
yum install -y gcc-c++ make-
curl -sL https://rpm.nodesource.com/setup_10.x | sudo -E bash -
yum -y install nodejs

# get node dependencies
npm install express
npm install mongodb

# open all necessary ports
sudo firewall-cmd --zone=public --permanent --add-port=3000/tcp
sudo firewall-cmd --zone=public --permanent --add-port=4000/tcp
sudo systemctl restart firewalld         
